export const environment = {
  production: true,
  apiEndpoint: "http://192.168.0.108:8000/api",
  apiSecureEndpoint: "http://192.168.0.108:8000/api/secure",
  endpoint: "http://192.168.0.108:8000"
};
