import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from '../admin/admin.component';

// Routes list
const routes: Routes = [
     {
          path: 'app',
          loadChildren: "../admin/admin.module#AdminModule"
     },
     {
          path: 'test',
          component: AdminComponent
     }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RouteRoutingModule { }
